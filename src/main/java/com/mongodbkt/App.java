package com.mongodbkt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

public class App 
{
	public static void main( String[] args )
    {
    	// Get Spring application context
        @SuppressWarnings("resource")
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("springApplicationContext.xml");
         
        DateFormat format = new SimpleDateFormat("MMMM d, yyyy hh:mm:ss z", Locale.ENGLISH);
        
        // Get bean instances
        MongoTemplate mongoTemplate = applicationContext.getBean("mongoTemplate", MongoTemplate.class);
        MongoOperations mongoOperation = (MongoOperations) mongoTemplate;
        
        ActivityDAOImpl activityDAOImpl = applicationContext.getBean("activityDAOImpl", ActivityDAOImpl.class);
        
        String collectionName = "activities";
        
        /*System.out.println("#### Insert 4 New Activities ####");
        String dateStr = "August 21, 2011 14:12:55 UTC";
        Date date = null;
        try { date = format.parse(dateStr); } catch (ParseException e) {}
        activityDAOImpl.insert(mongoTemplate, collectionName, "java_act_001", 10, "java001", "User", date);
        
        dateStr = "July 12, 2013 01:19:23 UTC";
        try { date = format.parse(dateStr); } catch (ParseException e) {}
        activityDAOImpl.insert(mongoOperation, collectionName, "java_act_002", 12, "java002", "Actor", date);
        
        dateStr = "December 12, 2014 17:12:18 UTC";
        try { date = format.parse(dateStr); } catch (ParseException e) {}
        activityDAOImpl.insert(mongoOperation, collectionName, "java_act_003", 15, "java003", "Actor", date);
        
        dateStr = "January 15, 2015 16:18:19 UTC";
        try { date = format.parse(dateStr); } catch (ParseException e) {}
        activityDAOImpl.insert(mongoTemplate, collectionName, "55116d0627f52e8d0a5d1c72", "java_act_004", 11, "java001", "Participant", date);*/
        
        List<Activity> allActivities = activityDAOImpl.findAll(mongoTemplate, collectionName);
        System.out.println("#### Find All Activities ####");
        for (Activity activity2 : allActivities) {
        	System.out.println(activity2);
		}
        
        // Retrieve document from Activities Collection
        System.out.println("#### Find Activity by Id 55116d0627f52e8d0a5d1c72 ####");
        Activity activity = activityDAOImpl.findById(mongoTemplate, collectionName, "55116d0627f52e8d0a5d1c72");
        System.out.println(activity);
        
        System.out.println("#### Find one Activity with actor name \"User\" ####");
        activity = activityDAOImpl.findOne(mongoTemplate, collectionName, "User");
        System.out.println(activity);
        
        System.out.println("#### Sort by Date DESC and then Limit to 2 ####");
        allActivities = activityDAOImpl.sortByPublishDate(mongoTemplate, collectionName);
        for (Activity activity2 : allActivities) {
        	System.out.println(activity2);
		}
        System.out.println("#### Find Activity with Max and Min points WITHOUT Aggregation ####");
        activity = activityDAOImpl.getActivityWithMaxPoints(mongoTemplate, collectionName);
        System.out.println(activity);
        activity = activityDAOImpl.getActivityWithMinPoints(mongoTemplate, collectionName);
        System.out.println(activity);
		
        System.out.println("#### Find Status of activities collection ####");
        allActivities = activityDAOImpl.findAll(mongoTemplate, collectionName);
        System.out.println("Count of all activities: " + allActivities.size());
        allActivities = activityDAOImpl.findAllActivitiesWithActorName(mongoTemplate, collectionName, "Actor");
        System.out.println("Count of all activities with actor Name - Actor: " + allActivities.size());
    }
}
