package com.mongodbkt;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

//@Document
public class Activity {

	@Id
	private ObjectId _id;
	@Field("activity_id")
	private String activityId;
	@Field("points")
	private int points;
	@Field("actor")
	private Actor actor;
	@Field("published")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date publishedDate;
	
	public Activity() {
		super();
	}

	public Activity(String activityId, int points, Actor actor,
			Date publishedDate) {
		super();
		this.activityId = activityId;
		this.points = points;
		this.actor = actor;
		this.publishedDate = publishedDate;
	}

	public Activity(String objectId, String activityId, int points,
			Actor actor, Date publishedDate) {
		super();
		this._id = new ObjectId(objectId);
		this.activityId = activityId;
		this.points = points;
		this.actor = actor;
		this.publishedDate = publishedDate;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
