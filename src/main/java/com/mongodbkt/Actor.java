package com.mongodbkt;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.mongodb.core.mapping.Field;

public class Actor {

	@Field("actor_id")
	private String actorId;

	@Field("displayName")
	private String displayName;

	public Actor() {
		super();
	}

	public Actor(String actorId, String displayName) {
		super();
		this.actorId = actorId;
		this.displayName = displayName;
	}

	public String getActorId() {
		return actorId;
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
